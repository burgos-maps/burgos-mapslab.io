burgos-maps.gitlab.io
---

Powered by [Hugo](https://gohugo.io/)

This proyect use [GitLab Pages](https://about.gitlab.com/product/pages/) to built the website.

Theme used: https://github.com/gonapps-org/hugo-apps-theme

## Data

https://aytoburgos.transparencialocal.gob.es

https://datosabiertos.jcyl.es/

https://analisis.datosabiertos.jcyl.es/

http://opendata.jcyl.es/ficheros/carto/

http://www.idecyl.jcyl.es/geonetwork/srv/eng/catalog.search#/home

https://datos.gob.es/

[EU OpenData Portal](https://data.europa.eu/euodp/en/data/)

https://www.ign.es/web/ign/portal/cbg-area-cartografia

http://data.regional.atmosphere.copernicus.eu/openwis-user-portal/srv/en/main.home

## License

GNU GENERAL PUBLIC LICENSE

For the original gitlab HUGO page: MIT License (MIT) (c) 2014 Spencer Lyon
