---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

Web dedicada a la publicación de mapas interactivos creados a partir de datos públicos abiertos.

### Contacto

Nos vemos en el [**fediverso**](https://es.wikipedia.org/wiki/Fediverso):

- ![alt text][logo3] [pixelfed](https://pixelfed.social/burgosMaps)
-   ![alt text][logo2] [peertube](https://peertube.social/accounts/burgosmaps/videos)
-  [![Logo Mastodon][logo1]](https://fosstodon.org/@burgosmaps)

[logo1]: /img/logo_mastodon.png "Logo Mastodon"
[logo2]: /img/logo_peertube.svg "Logo peertube"
[logo3]: /img/pixelfed-icon.png "Logo pixelfed"
